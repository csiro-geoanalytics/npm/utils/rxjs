import { Subject, BehaviorSubject, Observable } from "rxjs";
import { filter } from "rxjs/operators";

import _ from "lodash";


export class NamedEventManager
{

	private events: EventsIndex = {};

	registerSubject<T>(event: string | number): Event<T>
	{
		if (this.events[event])
			throw new Error(`Named event '${ event }' is already registered.`);

		const subject = new Subject<T>();
		return this.events[event] = { subject: subject, observable: subject.asObservable() };
	}

	registerBehaviorSubject<T>(event: string | number, initialValue?: T): Event<T>
	{
		if (this.events[event])
			throw new Error(`Named event '${ event }' is already registered.`);

		const subject = new BehaviorSubject<T>(initialValue);
		return this.events[event] = { subject: subject, observable: subject.asObservable() };
	}

	isRegistered(event: string | number): boolean
	{
		return !!this.events[event];
	}

	unregister(event: string | number): void
	{
		if (this.events[event])
			delete this.events[event];
	}

	unregisterAll(): void
	{
		_.forOwn(this.events, (__, k) => delete this.events[k]);
	}

	get length(): number
	{
		return _.keys(this.events).length;
	}

	emit<T>(event: string | number, next?: T): void
	{
		if (this.events[event])
			return this.events[event].subject.next(next);
		throw new Error(`Event '${ event }' is not registered.`);
	}

	getSubject<T>(event: string | number): Subject<T>
	{
		return this.events[event] ? this.events[event].subject : null;
	}

	getObservable<T>(event: string | number): Observable<T>
	{
		return this.events[event] ? this.events[event].observable : null;
	}

	/**
	 * A method similar to getObservable() with the exception that it filters out the initial 'undefined' value for BehaviorSubject
	 * observables. Subject observables are returned intact.
	 *
	 * @template T
	 * @param {(string | number)} event
	 * @returns {Observable<T>}
	 * @memberof NamedEventManager
	 */
	getEvent$<T>(event: string | number): Observable<T>
	{
		return (this.getSubject<T>(event) instanceof BehaviorSubject)
			? this.getObservable<T>(event).pipe(filter(v => v !== undefined))
			: this.getObservable<T>(event);
	}

	getLastValue<T>(event: string | number): T
	{
		if (!this.events[event])
			throw new Error(`Event '${ event }' is not registered.`);
		const subject = this.events[event].subject;
		if (!(subject instanceof BehaviorSubject))
			throw new Error(`Event '${ event }' is not a BehaviorSubject.`);
		return (<BehaviorSubject<T>>subject).getValue();
	}

}

export interface Event<T>
{
	subject: Subject<T>;
	observable: Observable<T>;
}

export interface TraceableEvent<T>
{
	source: any;
	event: T;
}

export interface EventsIndex
{
	[ name: string ]: Event<any>;
	[ name: number ]: Event<any>;
}
