import { NamedEventManager } from "../named-event-manager";


const enum Events
{
	EnumEvent
}

/*
 ######
 #     # ######  ####  #  ####  ##### #####    ##   ##### #  ####  #    #
 #     # #      #    # # #        #   #    #  #  #    #   # #    # ##   #
 ######  #####  #      #  ####    #   #    # #    #   #   # #    # # #  #
 #   #   #      #  ### #      #   #   #####  ######   #   # #    # #  # #
 #    #  #      #    # # #    #   #   #   #  #    #   #   # #    # #   ##
 #     # ######  ####  #  ####    #   #    # #    #   #   #  ####  #    #
*/

describe("Named event registration process", () => {

	const obj = new NamedEventManager();

	test("starts as an empty collection", () =>
	{
		expect(obj.length).toBe(0);
	});

	test("registers a Subject", () => {
		obj.registerSubject("EventOne");
		expect(obj.length).toBe(1);
	});

	test("duplicate Subject registration throws an error", () => {
		expect(() => obj.registerSubject("EventOne")).toThrow();
	});

	test("unregisters a registerBehaviorSubject", () => {
		obj.registerBehaviorSubject("EventTwo", "initial");
		expect(obj.length).toBe(2);
	});

	test("duplicate BehaviorSubject registration throws an error", () => {
		expect(() => obj.registerBehaviorSubject("EventTwo", "initial")).toThrow();
	});

	test("EventOne is registered", () => {
		expect(obj.isRegistered("EventOne")).toBeTruthy();
	});

	test("NonRegistered event is not registered", () => {
		expect(obj.isRegistered("NonRegistered")).toBeFalsy();
	});

	test("register event using an enumerator", () => {
		obj.registerSubject(Events.EnumEvent);
		expect(obj.isRegistered(Events.EnumEvent)).toBeTruthy();
	});

	test("unregister EnumEventOne", () => {
		obj.unregister(Events.EnumEvent);
		expect(obj.isRegistered(Events.EnumEvent)).toBeFalsy();
	});

	test("unregister all events", () => {
		obj.unregisterAll();
		expect(obj.length).toBe(0);
	});

	test("getLastValue() of an non-registered event throw an error", () =>
	{
		expect(() => obj.getLastValue(Events.EnumEvent)).toThrowError();
	});

});

/*
  #####
 #     # #    # #####       # ######  ####  #####
 #       #    # #    #      # #      #    #   #
  #####  #    # #####       # #####  #        #
       # #    # #    #      # #      #        #
 #     # #    # #    # #    # #      #    #   #
  #####   ####  #####   ####  ######  ####    #
*/

describe("Subject-based event handling", () => {

	const obj = new NamedEventManager();

	test("registers a Subject", () =>
	{
		obj.registerSubject(Events.EnumEvent);
		expect(obj.length).toBe(1);
	});

test("getLastValue() throws an error", () =>
	{
		expect(() => obj.getLastValue(Events.EnumEvent)).toThrowError();
	});

	test("gets Subject instance", () => {
		expect(obj.getSubject(Events.EnumEvent)).not.toBeNull();
	});

	test("gets Observable instance", () => {
		expect(obj.getObservable(Events.EnumEvent)).not.toBeNull();
	});

	test("gets Event instance", () =>
	{
		const event = obj.getEvent$(Events.EnumEvent);
		expect(event).not.toBeNull();
		expect(event).toBe(obj.getObservable(Events.EnumEvent));
	});

	test("emits an event", () => {
		const next = jest.fn();
		obj.getObservable(Events.EnumEvent).subscribe(next);
		obj.emit(Events.EnumEvent, "value");
		expect(next).toBeCalled();
	});

	test("emit fails on an unregistered event", () => {
		expect(() => obj.emit("unregistered", "value")).toThrowError();
	});

});

/*
 ######                                               #####
 #     # ###### #    #   ##   #    # #  ####  #####  #     # #    # #####       # ######  ####  #####
 #     # #      #    #  #  #  #    # # #    # #    # #       #    # #    #      # #      #    #   #
 ######  #####  ###### #    # #    # # #    # #    #  #####  #    # #####       # #####  #        #
 #     # #      #    # ###### #    # # #    # #####        # #    # #    #      # #      #        #
 #     # #      #    # #    #  #  #  # #    # #   #  #     # #    # #    # #    # #      #    #   #
 ######  ###### #    # #    #   ##   #  ####  #    #  #####   ####  #####   ####  ######  ####    #
*/

describe("BehaviorSubject-based event handling", () => {

	const obj = new NamedEventManager();

	test("registers a BehaviorSubject", () =>
	{
		obj.registerBehaviorSubject(Events.EnumEvent, "initial");
		expect(obj.length).toBe(1);
	});

	test("getLastValue() returns the initial value", () =>
	{
		expect(obj.getLastValue(Events.EnumEvent)).toBe("initial");
	});

	test("subscription to the Observable emits the initial value", () => {
		const next = jest.fn();
		obj.getObservable(Events.EnumEvent).subscribe(next);
		expect(next).toBeCalled();
	});

	test("emits an event", () => {
		const next = jest.fn();
		obj.getObservable(Events.EnumEvent).subscribe(next);
		obj.emit(Events.EnumEvent, "value");
		expect(next).toBeCalledTimes(2);
	});

});
