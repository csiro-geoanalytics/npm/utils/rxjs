import { SubscriptionManager } from "../subscription-manager";
import { of, NEVER, range, concat, throwError, Subject } from "rxjs";

import _ from "lodash";


describe("Observables registration process", () => {

	const subscriptions = new SubscriptionManager();

	test("starts as an empty collection", () =>
	{
		expect(subscriptions.length).toBe(0);
	});

	test("registers an observable", () => {
		subscriptions.register(NEVER, _.noop);
		expect(subscriptions.length).toBe(1);
	});

	test("unregisters an observable", () => {
		subscriptions.unregister(NEVER);
		expect(subscriptions.length).toBe(0);
	});

	test("registers two observables", () => {
		const complete = jest.fn();
		subscriptions.register(NEVER, _.noop);
		subscriptions.register(of(1), _.noop, _.noop, complete, { subscribeOnRegister: true });
		expect(subscriptions.length).toBe(2);
		expect(complete).toBeCalled();
	});

	test("unregisters all observables", () => {
		subscriptions.unregisterAll();
		expect(subscriptions.length).toBe(0);
	});

});

describe("Subscription manager", () => {

	test("subscribes upon registration when subscribeOnRegister=true", () => {
		const subscriptions = new SubscriptionManager();
		const next = jest.fn();
		subscriptions.register(
			of(1),
			{ next: next },
			{ subscribeOnRegister: true }
		);
		subscriptions.unsubscribeAll();
		expect(next).toBeCalled();
	});

	test("doesn't subscribe upon registration when subscribeOnRegister=false|unset", () => {
		const subscriptions = new SubscriptionManager();
		const next = jest.fn();
		subscriptions.register(
			of(1),
			{ next: next }
		);
		subscriptions.unsubscribeAll();
		expect(next).not.toBeCalled();
	});

	test("subscribes to pre-registered observables upon calling subscribeAll()", () => {
		const subscriptions = new SubscriptionManager();
		const next = jest.fn();
		subscriptions.register(range(0, 2), { next: next });
		subscriptions.subscribeAll();
		subscriptions.unsubscribeAll();
		expect(next).toBeCalledTimes(2);
	});

	test("subscribes to pre-registered observables except one", () => {
		const subscriptions = new SubscriptionManager();
		const
			nextOne		= jest.fn(),
			nextAnother = jest.fn(),
			except$		= of(1);

		subscriptions.register(of(1), { next: nextOne });
		subscriptions.register(except$, { next: nextAnother });
		subscriptions.subscribeAll([ except$ ]);
		subscriptions.unsubscribeAll();

		expect(nextOne).toBeCalled();
		expect(nextAnother).not.toBeCalled();
	});

	test("calls next and complete handlers as expected", () => {
		const subscriptions = new SubscriptionManager();
		const
			next		= jest.fn(),
			complete	= jest.fn();

		subscriptions.register(range(0, 3), { next: next, complete: complete });
		subscriptions.subscribeAll();
		subscriptions.unsubscribeAll();

		expect(next).toBeCalledTimes(3);
		expect(complete).toBeCalledTimes(1);
	});

	test("calls next and error handlers as expected", () => {
		const subscriptions = new SubscriptionManager();
		const
			next	= jest.fn(),
			error	= jest.fn();

		subscriptions.register(
			concat(range(0, 3), throwError("Error")),
			{ next: next, error: error }
		);
		subscriptions.subscribeAll();
		subscriptions.unsubscribeAll();

		expect(next).toBeCalledTimes(3);
		expect(error).toBeCalledTimes(1);
	});

	test("unsubscribes from a Subscription", () => {
		const
			subscriptions	= new SubscriptionManager(),
			subject			= new Subject(),
			next			= jest.fn();

		const subscription = subscriptions.register(
			subject.asObservable(),
			{ next: next },
			{ subscribeOnRegister: true }
		);
		subject.next();
		subscriptions.unsubscribe(subscription);
		subject.next();
		expect(next).toBeCalledTimes(1);
	});

	test("unsubscribes from an Observable", () => {
		const
			subscriptions	= new SubscriptionManager(),
			subject			= new Subject(),
			obs$			= subject.asObservable(),
			next			= jest.fn();

		subscriptions.register(
			obs$,
			{ next: next },
			{ subscribeOnRegister: true }
		);
		subject.next();
		subscriptions.unsubscribe(obs$);
		subject.next();
		expect(next).toBeCalledTimes(1);
	});

	test("subscribes to an unregistered Observable return NULL", () => {
		const subscriptions	= new SubscriptionManager();
		expect(subscriptions.subscribe(of(1))).toBeNull();
	});


	test("resubscribes to an existing Observable", () => {
		const
			subscriptions	= new SubscriptionManager(),
			obs$			= of(1),
			next			= jest.fn();

		subscriptions.register(obs$, { next: next }, { subscribeOnRegister: true });
		subscriptions.subscribe(obs$);

		expect(next).toBeCalledTimes(2);
	});

});
