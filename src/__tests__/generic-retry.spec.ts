import { GenericRetryOperation, GenericRetryStrategy } from "../generic-retry";
import { of, throwError } from "rxjs";
import { map, retryWhen, switchMap } from "rxjs/operators";


/*
              #####
             #     # ##### #####    ##   ##### ######  ####  #   #
             #         #   #    #  #  #    #   #      #    #  # #
              #####    #   #    # #    #   #   #####  #        #
                   #   #   #####  ######   #   #      #  ###   #
 ### ### ### #     #   #   #   #  #    #   #   #      #    #   #
 ### ### ###  #####    #   #    # #    #   #   ######  ####    #
*/

describe("GenericRetryStrategy operation", () => {

	test("completes successfully on first attempt", () => {
		const next = jest.fn();
		const operation = jest.fn().mockReturnValue(true);

		of(null)
			.pipe(
				map(operation),
				retryWhen(GenericRetryStrategy())
			)
			.subscribe(next);

		expect(next).toBeCalled();
	});

	test("succeeds after one retry", () => {

		let resolver	: (value?: unknown) => void;
		const
			promise		= new Promise(resolve => resolver = resolve),
			next		= jest.fn(() => resolver()),
			error		= jest.fn(() => resolver()),
			complete	= jest.fn()
		;

		// Fail once before returning positive result.
		const operation = jest.fn()
			.mockReturnValue(true)
			.mockReturnValueOnce(false);

		of(null)
			.pipe(
				switchMap(() => operation() ? of(true) : throwError({})),
				retryWhen(GenericRetryStrategy({ retryInterval: 10 }))
			)
			.subscribe({ next, error, complete });

		return promise.then(() => {
			expect(operation).toBeCalledTimes(2);
			expect(next).toBeCalled();
			expect(complete).toBeCalled();
		});
	});

	test("fails after default number (3) of retries", () => {

		let resolver	: (value?: unknown) => void;
		const
			promise		= new Promise(resolve => resolver = resolve),
			error		= jest.fn(() => resolver())
		;

		// Failing condition.
		const operation = jest.fn().mockReturnValue(false);

		of(null)
			.pipe(
				switchMap(() => operation() ? of(true) : throwError({})),
				retryWhen(GenericRetryStrategy({ retryInterval: 10 }))
			)
			.subscribe({ error: error });

		// Wait for retry operation to finish before asserting results.
		return promise.then(() => {
			expect(operation).toBeCalledTimes(4);
			expect(error).toBeCalled();
		});
	});

});

/*
             #######
             #     # #####  ###### #####    ##   ##### #  ####  #    #
             #     # #    # #      #    #  #  #    #   # #    # ##   #
             #     # #    # #####  #    # #    #   #   # #    # # #  #
             #     # #####  #      #####  ######   #   # #    # #  # #
 ### ### ### #     # #      #      #   #  #    #   #   # #    # #   ##
 ### ### ### ####### #      ###### #    # #    #   #   #  ####  #    #
*/

describe("GenericRetryOperation normal operation", () => {

	test("completes successfully on first attempt", async () => {
		const next = jest.fn();
		return GenericRetryOperation(() => true, { next })
			.then(() => expect(next).toBeCalled());
	});

	test("succeeds after one retry", async () => {

		const
			next		= jest.fn(),
			complete	= jest.fn()
		;

		// Fail once before returning positive result.
		const condition = jest.fn()
			.mockReturnValue(true)
			.mockReturnValueOnce(false)
		;

		return GenericRetryOperation(condition, { next, complete }, { retryInterval: 10 })
			.then(() => {
				expect(condition).toBeCalledTimes(2);
				expect(next).toBeCalled();
				expect(complete).toBeCalled();
			});
	});

});

describe("GenericRetryOperation exception handling", () => {

	test("fails after two retries", async () => {

		const
			error		= jest.fn(),
			condition	= jest.fn().mockReturnValue(false)
		;

		return GenericRetryOperation(condition, { error }, { retryInterval: 10, maxRetryAttempts: 2 })
			.catch(() => {
				expect(condition).toBeCalledTimes(3);
				expect(error).toBeCalled();
			});
	});

	test("fails after default number (3) of retries", async () => {

		const
			error		= jest.fn(),
			complete	= jest.fn(),
			condition	= jest.fn().mockReturnValue(false)
		;

		return GenericRetryOperation(condition, { error, complete }, { retryInterval: 10 })
			.catch(() => {
				expect(condition).toBeCalledTimes(4);
				expect(error).toBeCalled();
				expect(complete).not.toBeCalled();
			});
	});

});
