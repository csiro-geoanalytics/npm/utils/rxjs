import { Observable, timer, of, throwError, PartialObserver } from "rxjs";
import { mergeMap, map, retryWhen, catchError, tap } from "rxjs/operators";


export interface GenericRetryStrategyOptions
{
	maxRetryAttempts?		: number;
	retryInterval?			: number;
	scalingDuration?		: number;
}

export const GenericRetryStrategy = (
	{
		// Default retry strategy.
		maxRetryAttempts	= 3,
		retryInterval		= 1000,
		scalingDuration		= 0

	}: GenericRetryStrategyOptions = {}
) => (attempts: Observable<any>): Observable<number> => {
	return attempts.pipe(
		mergeMap((error, i) => {
			const retryAttempt = i + 1;
			if (retryAttempt > maxRetryAttempts)
				throw error;

			return timer(retryInterval + (retryAttempt - 1) * scalingDuration);
		})
	);
};

export const GenericRetryOperation = (
	condition	: () => boolean,
	observer	: PartialObserver<void>,
	options?	: GenericRetryStrategyOptions
): Promise<void> =>
{
	let resolver			: (value?: unknown) => void;
	let rejector			: (reason?: any) => void;
	const promise			= new Promise<void>((resolve, reject) => { resolver = resolve; rejector = reject; });

	of(null)
		.pipe(
			map(() => {
				if (!condition())
					throw new Error("Condition of GenericRetryOperation is still unsatisfied.");
			}),
			retryWhen(GenericRetryStrategy(options)),
			catchError(err => {
				rejector(err);
				return throwError(err);
			}),
			tap(() => resolver())
		)
		.subscribe(observer);
	return promise;
};
