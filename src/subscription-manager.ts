import { Subscription, Observable, PartialObserver } from "rxjs";

import _ from "lodash";


export interface ActionableObservable<T>
{
	observable				: Observable<T>;
	observer				: PartialObserver<T>;
}

export interface SubscriptionOptions
{
	subscribeOnRegister?	: boolean;
}

interface SubscriptionItem<T> extends ActionableObservable<T>
{
	subscription?			: Subscription;
}

const DEFAULT_OPTIONS = <SubscriptionOptions>{
	subscribeOnRegister		: false
};

export class SubscriptionManager
{
	protected subscriptions: SubscriptionItem<any>[] = [];

	register<T>(observable: Observable<T>, nextOrObserver?: ((value: T) => void) | PartialObserver<T>, options?: SubscriptionOptions): Subscription;
	register<T>(observable: Observable<T>, next?: (value: T) => void, error?: (error: any) => void, complete?: () => void, options?: SubscriptionOptions): Subscription;
	// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
	register<T>(observable: Observable<T>, nextOrObserver?: ((value: T) => void) | PartialObserver<T>, ...params: any): Subscription
	{
		let
			next		: (value: T) => void,
			error		: (error: any) => void,
			complete	: () => void,
			options		: SubscriptionOptions;

		// Parse overloaded method arguments.
		if (nextOrObserver instanceof Function)
		{
			if (params[0] instanceof Function || params.length > 1)
				[ next, error, complete, options ] = [ <(value: T) => void>nextOrObserver, ...params ];
			else
				[ next, error, complete, options ] = [ <(value: T) => void>nextOrObserver, undefined, undefined, ...params ];
		}
		else
		{
			const observer = <PartialObserver<T>>nextOrObserver;
			[ next, error, complete, options ] = [ observer.next, observer.error, observer.complete, ...params ];
		}

		// Apply defaults.
		options = { ...DEFAULT_OPTIONS, ...options };

		// Push condigured observable into the data array.
		this.subscriptions.push(<SubscriptionItem<T>>{
			observable			: observable,
			observer: {
				next			: next,
				error			: (e: any) => { this.unsubscribe(observable); (error || _.noop)(e); },
				complete		: () => { this.unsubscribe(observable); (complete || _.noop)(); }
			}
		});
		return options.subscribeOnRegister ? this.subscribe(observable) : null;
	}

	unregister<T>(observable: Observable<T>): void
	{
		const item = this.unsubscribe(observable);
		if (item)
			_.remove(this.subscriptions, v => v === item);
	}

	unregisterAll(): void
	{
		if (this.subscriptions && this.subscriptions.length)
		{
			this.unsubscribeAll();
			this.subscriptions = [];
		}
	}

	subscribe<T>(observable: Observable<T>): Subscription
	{
		const item = _.find(this.subscriptions, (v: SubscriptionItem<T>) => v.observable === observable);
		if (item)
		{
			if (item.subscription)
				item.subscription.unsubscribe();
			return item.subscription = item.observable.subscribe(item.observer);
		}
		return null;
	}

	subscribeAll(except?: Observable<any>[]): void
	{
		_.each(this.subscriptions, item => {
			if (!item.subscription && !(except && except.includes(item.observable)))
				item.subscription = item.observable.subscribe(item.observer);
		});
	}

	unsubscribe<T>(src: Observable<T> | Subscription): SubscriptionItem<T>
	{
		if (src instanceof Subscription)
		{
			const item = _.find(this.subscriptions, (v: SubscriptionItem<any>) => v.subscription === src);
			(<Subscription>src).unsubscribe();
			if (item)
				item.subscription = null;
			return item;
		}
		else
		{
			const item = _.find(this.subscriptions, (v: SubscriptionItem<T>) => v.observable === src);
			if (item && item.subscription)
			{
				item.subscription.unsubscribe();
				item.subscription = null;
			}
			return item;
		}
	}

	unsubscribeAll(): void
	{
		_.each(this.subscriptions, v => {
			if (v.subscription)
			{
				v.subscription.unsubscribe();
				v.subscription = null;
			}
		});
	}

	get length(): number
	{
		return this.subscriptions.length;
	}
}
